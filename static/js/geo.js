var geocoder;
var marker;
var map;
var lat, lng;
var latlng;

function initialize() {
	if(document.getElementById("map-canvas")){
		geocoder = new google.maps.Geocoder();
		var lat = document.getElementById('input-ramp-coords-lat').value;
		var lng = document.getElementById('input-ramp-coords-lng').value;

		if (!lat && !lng){
			latlng = new google.maps.LatLng(45.3269044, 14.44200120000005);
		}else{
			latlng = new google.maps.LatLng(lat, lng);
			codeAddress();
		}
		var mapOptions = {
			zoom: 14,
			center: latlng
		}
		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	}
}

function codeAddress() {
  var address = document.getElementById('address').value;
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      placeMarker(results[0].geometry.location);
      lat = (results[0].geometry.location.lat());
      lng = (results[0].geometry.location.lng());
      document.getElementById('input-ramp-coords-lat').value = lat;
	  document.getElementById('input-ramp-coords-lng').value = lng;
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

function placeMarker(location) {
    if (marker) {
        //if marker already was created change positon
        marker.setPosition(location);
    } else {
        //create a marker
        marker = new google.maps.Marker({
            position: location,
            map: map,
            draggable: true
        });
    }
}

function getLatLng() {

}

google.maps.event.addDomListener(window, 'load', initialize);
