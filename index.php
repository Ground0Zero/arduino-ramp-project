<?php
$loader = require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Capsule\Manager as DB;


//php cors
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

$capsule = new Capsule;

$capsule->addConnection(array(
	'driver'    => 'mysql',
	'host'      => '127.0.0.1',
	'database'  => 'arduino_ramp',
	'username'  => 'root',
	'password'  => 'root',
	'charset'   => 'utf8',
	'collation' => 'utf8_unicode_ci',
	'prefix'    => ''
));

$capsule->setAsGlobal();
$capsule->bootEloquent();

use \App\Models\User;
use \App\Models\Ramp;
use \App\Models\Access;
use \App\Models\Action;

//use \Slim\Middleware\HttpBasicAuth;
//$app->add(new HttpBasicAuth('theUsername', 'thePassword'));

$app = new \Slim\Slim(array(
	'view' => new \Slim\Views\Smarty(),
	'debug' => true,
	//'log.enabled' => true,
	//'log.level' => \Slim\Log::WARN,
));

//use \Slim\Middleware\StrongAuth;
/*
$strongConfig = array(
	'provider' => 'PDO',
	'pdo' => new PDO('mysql:host=localhost;dbname=arduino_ramp', 'root', 'root'),
	'auth.type' => 'form',
	'login.url' => '/',
	'security.urls' => array(
		array('path' => '/access'),
		array('path' => '/users/.+'),
	),
);
*/
//$app->add(new StrongAuth($strongConfig));


$app->add(new \Slim\Middleware\SessionCookie(array(
	'expires' => '20 minutes',
	'path' => '/',
	'domain' => null,
	'secure' => false,
	'httponly' => false,
	'name' => 'slim_session',
	'secret' => 'myappsecret',
	'cipher' => MCRYPT_RIJNDAEL_256,
	'cipher_mode' => MCRYPT_MODE_CBC
)));

$view = $app->view();
$view->parserDirectory = dirname(__FILE__) . '/libs/smarty/';
$view->parserCompileDirectory = dirname(__FILE__) . '/tmp/compiled';
$view->parserCacheDirectory = dirname(__FILE__) . '/tmp/cache';

$view->setData(array(
	'sitename' => 'Ramp Control',
	'route' => $app->request->getResourceUri(),
));

error_reporting(E_ALL);
ini_set('display_errors', true);
function shutdown() {
	$error = error_get_last();
	if (!empty($error)) {
		print_r($error);
	}
}
register_shutdown_function('shutdown');

$app->error(function (\Exception $e) use ($app) {
	$app->render('error.php');
});

$authenticate = function ($app) {
	return function () use ($app) {
		if (!isset($_SESSION['user'])) {
			$_SESSION['urlRedirect'] = $app->request()->getPathInfo();
			$app->flash('error', 'Login required');
			$app->redirect('/login');
		}
	};
};
$app->hook('slim.before.dispatch', function() use ($app) {
		$user = null;
		if (isset($_SESSION['user'])) {
			$user = $_SESSION['user'];
		}
		$app->view()->setData('user', $user);
});

/*--- GET ROUTES  ---*/

$app->get('/', function () use($app) {
	$app->redirect('/login');
});

$app->get("/login", function () use ($app) {
	$flash = $app->view()->getData('flash');
	$error = '';
	if (isset($flash['error'])) {
		$error = $flash['error'];
	}
	$urlRedirect = '/access';
	if ($app->request()->get('r') && $app->request()->get('r') != '/logout' && $app->request()->get('r') != '/login') {
		$_SESSION['urlRedirect'] = $app->request()->get('r');
	}
	if (isset($_SESSION['urlRedirect'])) {
		$urlRedirect = $_SESSION['urlRedirect'];
	}
	$email_value = $email_error = $password_error = $credentials_error = '';
	//if (isset($flash['email'])) {
		//$email_value = $flash['email'];
	//}
	if (isset($flash['errors']['email'])) {
		$email_error = $flash['errors']['email'];
	}
	if (isset($flash['errors']['password'])) {
		$password_error = $flash['errors']['password'];
	}

	$app->render('tpl/login.tpl', array('error' => $error, 'email_error' => $email_error, 'password_error' => $password_error, 'urlRedirect' => $urlRedirect));
});

$app->post("/login", function () use ($app) {

	$email = $app->request()->post('email');
	$password = $app->request()->post('password');
	$errors = array();
	$user_found = array();

	$user_found = \App\Models\User::findByMail($email);

	if ( empty($user_found) ){
		$user_found = 0;
	}

	if( empty($user_found[0]['mail']) && $email != $user_found[0]['mail'] ){
		$errors['email'] = "Invalid username!";
	}
	else if ($password != $user_found[0]['pass'] ) {
		$errors['password'] = "Invalid password!";
		//$errors['credentials'] = "Invalid credentials!";
		//$app->halt(401, "Invalid login");
	}
	if (count($errors) > 0) {
		$app->flash('errors', $errors);
		$app->redirect('/login');
	}
	$_SESSION['user'] = $email;
	if (isset($_SESSION['urlRedirect'])) {
		$tmp = $_SESSION['urlRedirect'];
		unset($_SESSION['urlRedirect']);
		$app->redirect($tmp);
	}

	$app->redirect('/access');

});

$app->get('/access', $authenticate($app), function () use($app) {

	$users = \App\Models\User::all();
	$ramps = \App\Models\Ramp::all();
	$access = \App\Models\Access::all();

	$app->render('tpl/access.tpl', array('title' => 'Ramp Access', 'active' => '/access', 'users' => $users, 'ramps' => $ramps, 'access' => $access));
});

$app->get('/users', $authenticate($app), function () use($app) {

	$users = \App\Models\User::all();
	$app->response()->status(200);
	$app->render('tpl/users.tpl', array('title' => 'Users', 'active' => '/users', 'users' => $users));
});

$app->get('/ramps', $authenticate($app), function () use($app) {

	$ramps = \App\Models\Ramp
		::getWithCoordinates()
		->get()
	;

	$app->response()->status(200);
	$app->render('tpl/ramps.tpl', array('active' => '/ramps', 'ramps' => $ramps));

});

$app->get('/logs', $authenticate($app), function () use($app) {

	$logs = \App\Models\Log::all();
	$app->response()->status(200);
	$app->render('tpl/logs.tpl', array('title' => 'Logs', 'active' => '/logs', 'logs' => $logs));
});

$app->get("/logout", function () use ($app) {
	unset($_SESSION['user']);
	$app->view()->setData('user', null);
	//$app->render('logout.php');
	$app->redirect('/login');
});

$app->get('/access/:id', $authenticate($app), function ($id) use($app) {

	$access = \App\Models\Access::findOrFail($id);

	$app->response()->status(200);
	$app->render('tpl/access.tpl', array('active' => '/access', 'access' => $access));

});

$app->get('/users/:id', $authenticate($app), function ($id) use($app) {

	$user = \App\Models\User::findOrFail($id);

	$app->response()->status(200);
	$app->render('tpl/user.tpl', array('active' => '/users', 'user' => $user));

});

$app->get('/ramps/:id', $authenticate($app), function ($id) use($app) {

	$ramp = \App\Models\Ramp
		::getWithCoordinates()
		->where('ramp_id', '=', $id)
		->first()
	;

	$app->response()->status(200);
	$app->render('tpl/ramp.tpl', array('active' => '/ramps', 'ramp' => $ramp));
});

/*--- POST ROUTES - list all models ---*/

$app->post('/access', $authenticate($app), function () use ($app) {

	$access = \App\Models\Access::all();

	if (isset($_POST["deleteUserAccess"])) {
		$id = $_POST["deleteUserAccess"];
		//var_dump($id);
		$access = \App\Models\Access::find($id);
		$access->delete();
		return $app->response->redirect('/access', 301);
	}

	$allPostVars = $app->request->post();
	$access = Access::create($allPostVars);
	$app->flash('newUserAccessAdded', '1');
	$app->response->redirect('/access', 301);

});

$app->delete("/access/:id", $authenticate($app), function ($id) use ($app){

	$access = \App\Models\Access::findOrFail($id);
	$access->delete();
	$app->flash('userAccessDeleted', '1');
	return $app->response->redirect('/access', 301);
});

$app->post('/users', $authenticate($app), function () use ($app) {

	$allPostVars = $app->request->post();
	$user = User::create($allPostVars);
	$app->flash('newUserAdded', '1');
	$app->response->redirect('/users', 301);

});

$app->post('/ramps', $authenticate($app), function () use ($app) {

	$allPostVars = $app->request->post();
	$allPostVars['map_coordinates'] = null;
	$ramp = Ramp::create($allPostVars);
	$app->flash('newRampAdded', '1');
	$app->response->redirect('/ramps', 301);

});

/*--- POST ROUTES - list models by ID ---*/

$app->post('/users/:id', $authenticate($app), function ($id) use ($app) {

	$user = \App\Models\User::find($id);

	if (isset($_POST["deleteUser"])) {
		$user->delete();
		$app->flash('userDeleted', '1');
		$app->response->redirect('/users', 301);
	}

	$allPostVars = $app->request->post();
	$user->fill($allPostVars)->save();
	$app->flash('userChanged', '1');
	$app->response->redirect('/users/' . $id, 301);

});

$app->post('/ramps/:id', $authenticate($app), function ($id) use ($app) {

	$ramp = \App\Models\Ramp::findOrFail($id);

	if (isset($_POST["deleteRamp"])) {
		$ramp->delete();
		return $app->response->redirect('/ramps', 301);
	}

	$allPostVars = $app->request->post();
	$allPostVars['map_coordinates'] = null;
	$ramp->fill($allPostVars)->save();
	$app->flash('rampChanged', '1');
	$app->response->redirect('/ramps/' . $id, 301);

});

/* API controllers for mobile communication */

$app->get('/api/access', function () use($app) {

	$access = \App\Models\Access::all();

	if ($app->request->isAjax()) {
		return $app->response->headers->set('Content-Type', 'application/json')->setBody($access->toJson())->setStatus(200)->finalize();
	}
});

$app->get('/api/users', function () use($app) {

	$users = \App\Models\User::all();

	if ($app->request->isAjax()) {
		return $app->response->headers->set('Content-Type', 'application/json')->setBody($users->toJson())->setStatus(200)->finalize();
	}
});

$app->get('/api/ramps',  function() use($app) {

	$ramps = \App\Models\Ramp::getWithCoordinates();

	/*
	if ($app->request->get('ramp_id')) {
		$ramps->where('ramp_id', '=', $app->request->get('ramp_id'));
	}*/
	$ramps = $ramps->get();
		$app->response->setBody($ramps->toJson());
		$app->response->headers->set('Content-Type', 'application/json');

});

$app->get('/api/actions', function () use($app) {
	$actions = \App\Models\Action::select('*');

	if ($app->request->get('ramp_id')) {
		$actions->where('ramp_id', '=', $app->request->get('ramp_id'));
	}
	if ($app->request->get('user_id')) {
		$actions->where('user_id', '=', $app->request->get('user_id'));
	}

	if ($app->request->get('action_status')) {
		$actions->where('action_status', '=', $app->request->get('action_status'));
	}

	$actions = $actions->get();

	//if ($app->request->isAjax()) {
		//$app->response()->status(200);
		//$app->response->setStatus(200);
		$app->response->setBody($actions->toJson());
		$app->response->headers->set('Content-Type', 'application/json');
	//}
});

$app->post('/api/actions', function () use ($app) {

	//$action = new \App\Models\Action;
	$action = \App\Models\Action::all();

//	$ramps = \App\Models\Ramp::all();
//	$users = \App\Models\User::all();
/*
	if ($app->request->get('action_type')) {
		 $action_type = $app->request->get('action_type');
	}
	if ($app->request->get('ramp_id')) {
		$ramp_id = $app->request->get('ramp_id');
	}*/
	//if ($app->request->get('user_id')) {
		//$sent_user_id = \App\Models\User::find($app->request->get('user_id'));
		/*if ($sent_user_id){
			$user_id_ok = $sent_user_id;
		}
		else{
			echo'user ID not found message';
		}*/
	//}

	//$action->action_type = $action_type;
	//$action->ramp_id = $ramp_id;
	//$action->user_id = $user_id_ok;

	//$action->save();

	$allPostVars = $app->request->post();
	file_put_contents('text.txt', print_r($app->request->post(), 1).print_r($app->request->get(), 1));
	$action = Action::create($allPostVars);

	return $app->response->headers->set('Content-Type', 'application/json')->setBody($action->toJson())->setStatus(200)->finalize();
});

$app->run();

?>