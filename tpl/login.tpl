{extends file='tpl/index-without-nav.tpl'}

{block name=title}
	Login
{/block}

{block name=content}
	<section id="login">
		<div class="container">
			<div class="card card-container">
				<img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
				<p id="profile-name" class="profile-name-card"></p>
				<form method="post" class="form-signin">
					<span id="reauth-email" class="reauth-email"></span>
					<input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
					<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
					<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" value="Login">Login</button>
				</form>
				{if !empty($error)}
					<p class="danger-msg-red">{$error}</p>
				{/if}
				{if !empty($email_error)}
					<p class="danger-msg-red">{$email_error}</p>
				{/if}
				{if !empty($password_error)}
					<p class="danger-msg-red">{$password_error}</p>
				{/if}
			</div>
		</div>
	</section>
{/block}