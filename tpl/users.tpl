{extends file='tpl/index.tpl'}

{block name=title}
	Users
{/block}

{block name=content}
<section id="users">
	<div class="container">
		<div class="col-md-12 table-responsive">
			<h1>All users:</h1>
			<table class="table table-bordered table-hover">
				<tr>
					<!--<th class="tg-s6z2" colspan="8">Results</th>-->
					<tr class="tg-s6z2" colspan="6">
						<td>ID</td>
						<td>Type</td>
						<td>Username / mail</td>
						<td>Password</td>
						<td>Telephone number</td>
						<td>Created on</td>
						<td>Updated at</td>
						<td></td>
					</tr>
				</tr>
				{foreach from=$users item=i}
					<tr>
						<td class="tg-vn2c">{$i.user_id}</td>
						<td class="tg-vn4c">{$i.user_type}</td>
						<td class="tg-vn4c">{$i.mail}</td>
						<td class="tg-vn4c">{$i.pass}</td>
						<td class="tg-vn4c">{$i.tel_number}</td>
						<td class="tg-vn4c">{$i.created_at}</td>
						<td class="tg-vn4c">{$i.updated_at}</td>
						<td class="tg-vn4c"><a href="/users/{$i.user_id}" class="btn btn-danger">Edit</a></td>
					</tr>
				{/foreach}
			</table>
		</div>
		<div class="row col-md-6">
			<div class="col-md-12">
				<div class="well well-sm">
					<form class="form-horizontal" method="post">
						<fieldset>
							<legend class="header col-md-10 col-md-offset-1 form-main-title">Add new user</legend>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-user-type">User type:</label>
									<select name="user_type" id="input-user-type" class="form-control" required>
										<option value="user">User</option>
										<option value="admin">Admin</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-user-email">Email address:</label>
									<input id="input-user-email" name="mail" type="text" placeholder="" class="form-control" required>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-user-pass">Password:</label>
									<input id="input-user-pass" name="pass" type="password" placeholder="" class="form-control" required>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-user-tel">Telephone number:</label>
									<input id="input-user-tel" name="tel_number" type="text" placeholder="" class="form-control" required>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<button type="submit" id="submit-new-user" class="btn btn-primary btn-lg">Submit</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>

		{block name=response}
			<div class="status-message col-md-6">
				{if $flash.newUserAdded == 1}
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<strong>Success!</strong> New user added to database.
					</div>
				{elseif $flash.userDeleted == 1}
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<strong>Success!</strong> User deleted from database.
					</div>
				{/if}
			</div>
		{/block}
	</div>
</section>
{/block}


