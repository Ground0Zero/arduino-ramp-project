{extends file='tpl/index.tpl'}

{block name=title}
	Ramp details
{/block}

{block name=content}
<section id="ramp">
	<div class="container">
		<div class="col-md-12 table-responsive">
			<h1>Edit ramp:</h1>
		</div>
		<div class="row col-md-6">
			<div class="col-md-12">
				<div class="well well-sm">
					<form class="form-horizontal" method="post">
						<fieldset>
							<legend></legend>
							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-ramp-name">Ramp name:</label>
									<input id="input-ramp-name" name="ramp_name" type="text" class="form-control" value="{$ramp['ramp_name']}">
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-ramp-tel">Ramp telephone number:</label>
									<input id="input-ramp-tel" name="ramp_tel_number" type="tel" class="form-control" value="{$ramp['ramp_tel_number']}">
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-ramp-interval">Ramp time delay interval (in seconds):</label>
									<input id="input-ramp-interval" name="interval" type="number" class="form-control" value="{$ramp['interval']}">
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-ramp-status">Ramp status:</label>
									<select name="status" class="form-control" required>
										{if $ramp['status'] == 0}
											<option value=0>Inactive</option>
											<option value=1>Active</option>
										{else}
											<option value=1>Active</option>
											<option value=0>Inactive</option>
										{/if}
									</select>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="address">Ramp address:</label>
									<div class="input-group">
										<input id="address" name="location" placeholder="Ramp location" class="form-control" value="{$ramp['location']}">

										<span class="input-group-btn">
											<button id="get-location" class="btn btn-default" type="button" value="Geocode" onclick="codeAddress()"><i class="fa fa-location-arrow"></i></button>
										</span>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label class="input-title" for="input-ramp-coords">Ramp coordinates:</label>
									<input id="input-ramp-coords-lat" name="map_coordinates_latitude" type="text" placeholder="Latitude" class="form-control col-md-6 col-xs-12" value="{$ramp['map_coordinates_latitude']}">
									<input id="input-ramp-coords-lng" name="map_coordinates_longitude" type="text" placeholder="Longitude" class="form-control col-md-6 col-xs-12" value="{$ramp['map_coordinates_longitude']}">
									<div class="clear"></div>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<button type="submit" class="btn btn-primary btn-lg">Save changes</button>
									<button type="submit" class="btn btn-lg btn-danger" name="deleteRamp" value="1">Delete ramp</button>
								</div>
							</div>

						</fieldset>
					</form>
				</div>
			</div>
		</div>

		<div id="map-canvas" class="col-md-6"></div>

		{block name=response}
			<div class="status-message col-md-6">
				{if $flash.rampChanged == 1}
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<strong>Success!</strong> Ramp data has been updated.
					</div>
				{/if}
			</div>
		{/block}
	</div>
</section>
{/block}


