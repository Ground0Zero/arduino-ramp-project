<!DOCTYPE html>
<html lang="en">
	<head>
		<TITLE>{$sitename} - {block 'title'}{/block}</TITLE>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Bootstrap core CSS, Font Awesome, Custom styles -->
		<link href="/static/css/bootstrap.min.css" rel="stylesheet">
		<link href="/static/css/font-awesome.min.css" rel="stylesheet">
		<link href="/static/css/style.css" rel="stylesheet">

		<!-- Geo js -->
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
		<script src="/static/js/geo.js"></script>

	</head>

	<body class="page-{ltrim($route, '/')}" data-route="{$route}">

	<nav class="navbar navbar-inverse" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li {if ($active == '/access')} class="active"{/if}><a href="/access">Access</a></li>
					<li {if ($active == '/users')} class="active"{/if}><a href="/users">Users</a></li>
					<li {if ($active == '/ramps')} class="active"{/if}><a href="/ramps">Ramps</a></li>
					<li {if ($active == '/logs')} class="active"{/if}><a href="/logs">Logs</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a>{$user}</a></li>
					<li><a href="/logout"><i class="fa fa-power-off"></i> Logout</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div id="content">
			{block 'content'}{/block}
	</div>

	<footer id="footer">
		<div class="wrap">
			<a href="/" target="_blank" class="footer-logo-1"></a>
			<a href="/" target="_blank" class="footer-logo-2"></a>
			<a href="/" target="_blank" class="footer-logo-3"></a>
			<a href="/" target="_blank" class="footer-logo-4"></a>
		</div>
	</footer>

	</body>

	<!-- jQuery, Bootstrap core JavaScript, Bootbox
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="/static/js/jquery-1.11.3.min.js"></script>
	<script src="/static/js/bootstrap.min.js"></script>
	<script src="/static/js/main.js"></script>
</html>
