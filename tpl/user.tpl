{extends file='tpl/index.tpl'}

{block name=title}
	User info
{/block}

{block name=content}
<section id="user">
	<div class="container">
		<div class="col-md-12 table-responsive">
			<h1>Edit user:</h1>{print_r($user)}
		</div>
		<div class="row col-md-6">
			<div class="col-md-12">
				<div class="well well-sm">
					<form class="form-horizontal" method="post">
						<fieldset>
							<legend></legend>
							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-user-type">User type:</label>
									<select name="user_type" class="form-control" required>
										{if $user['user_type'] == 'admin'}
											<option value="admin">admin</option>
											<option value="user">user</option>
										{else}
											<option value="user">user</option>
											<option value="admin">admin</option>
										{/if}
									</select>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-user-email">Email address:</label>
									<input id="email" name="mail" type="text" placeholder="Email Address" class="form-control" value="{$user['mail']}" required>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-user-pass">Password:</label>
									<input id="pass" name="pass" type="text" placeholder="Password" class="form-control" value="{$user['pass']}" required>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-user-tel">Telephone number:</label>
									<input id="tel" name="tel_number" type="text" placeholder="Telephone number" class="form-control" value="{$user['tel_number']}" required>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<button type="submit" class="btn btn-primary btn-lg">Save changes</button>
									<button type="submit" class="btn btn-lg btn-danger" name="deleteUser" value="1">Delete user</button>
								</div>
							</div>

						</fieldset>
					</form>
				</div>
			</div>
			{block name=response}
			<div class="status-message col-md-12">
				{if $flash.userChanged == 1}
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<strong>Success!</strong> User data has been updated.
					</div>
				{/if}
			</div>
		{/block}
		</div>
	</div>
</section>
{/block}


