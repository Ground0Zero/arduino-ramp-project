{extends file='tpl/index.tpl'}

{block name=title}
	Logs
{/block}

{block name=content}
<section id="logs">
	<div class="container">
		<div class="col-md-12 table-responsive">
			<h1>Action logs:</h1>
			<table class="table table-bordered table-hover">
				<tr>
					<!--<th class="tg-s6z2" colspan="8">Results</th>-->
					<tr class="tg-s6z2" colspan="6">
						<td>Action log ID</td>
						<td>Action ID</td>
						<td>Action result</td>
						<td>Action details</td>
					</tr>
				</tr>
				{foreach from=$logs item=i}
					<tr>
						<td class="tg-vn2c">{$i.action_log_id}</td>
						<td class="tg-vn4c">{$i.action_id}</td>
						<td class="tg-vn4c">{$action_log_result}</td>
						<td class="tg-vn4c">{$i.action_log_details}</td>
					</tr>
				{/foreach}
			</table>
		</div>
	</div>
</section>
{/block}


