{extends file='tpl/index.tpl'}

{block name=title}
	Access
{/block}

{block name=content}
	<section id="accesses">
		<div class="container">
			<div class="col-md-12 table-responsive">
				<h1>Ramp access:</h1>
				<table class="table table-bordered table-hover">
					<tr>
						<tr class="tg-s6z2" colspan="6">
							<td>Access ID</td>
							<td>User</td>
							<td>Ramp</td>
							<td>Location</td>
							<td>Permission</td>
							<td>Created on</td>
							<td>Updated at</td>
							<td></td>
						</tr>
					</tr>
					{foreach from=$access item=i}
						<tr>
							<td class="tg-vn4c">{$i.access_id}</td>
							<td class="tg-vn2c">
								{foreach from=$users item=u}
									{if $u.user_id == $i.user_id}
										{$u.mail}
									{/if}
								{/foreach}
							</td>
							<td class="tg-vn4c">{foreach from=$ramps item=r}
									{if $r.ramp_id == $i.ramp_id}
										{$r.ramp_name}
									{/if}
								{/foreach}</td>
							<td class="tg-vn4c">{foreach from=$ramps item=r}
									{if $r.ramp_id == $i.ramp_id}
										{$r.location}
									{/if}
								{/foreach}</td>
							<td class="tg-vn4c">{$i.user_permission}</td>
							<td class="tg-vn4c">{$i.created_at}</td>
							<td class="tg-vn4c">{$i.updated_at}</td>
							<td class="tg-vn4c">
								<form action="access/{$i.access_id}" method="post" onsubmit="return confirm('Are you sure You want to delete this user access?')">
									<button type="submit" class="btn btn-danger">Remove</button>
									<input type="hidden" name="_METHOD" value="DELETE"/>
								</form>
							</td>
						</tr>
					{/foreach}
				</table>
			</div>
			<div class="row col-md-6">
				<div class="col-md-12">
					<div class="well well-sm">
						<form class="form-horizontal" method="post">
							<fieldset>
								<legend class="header col-md-10 col-md-offset-1 form-main-title">Assign new user access</legend>
								<div class="form-group">
									<div class="col-md-10 col-md-offset-1">
										<label for="input-user-type">User:</label>
										<select name="user_id" class="form-control" required>
											{foreach from=$users item=i}
												<option value="{$i.user_id}">{$i.mail}</option>
											{/foreach}
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-10 col-md-offset-1">
										<label for="input-user-type">Ramp:</label>
										<select name="ramp_id" class="form-control" required>
											{foreach from=$ramps item=i}
												<option value="{$i.ramp_id}">{$i.ramp_name}</option>
											{/foreach}
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-10 col-md-offset-1">
										<label for="input-user-type">User permission:</label>
										<select name="user_permission" class="form-control" required>
											<option value="user">User</option>
											<option value="owner">Owner	</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-10 col-md-offset-1">
										<button type="submit" id="submit-new-user-access" class="btn btn-primary btn-lg">Submit</button>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			{block name=response}
				<div class="status-message col-md-6">
					{if $flash.newUserAccessAdded == 1}
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<strong>Success!</strong> New user access added to database.
						</div>
					{elseif $flash.userAccessDeleted == 1}
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<strong>Success!</strong> User access removed from database.
						</div>
					{/if}
				</div>
			{/block}
		</div>
	</section>
{/block}
