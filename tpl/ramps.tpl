{extends file='tpl/index.tpl'}

{block name=title}
	<title>Ramps</title>
{/block}

{block name=content}
<section id="ramps">
	<div class="container">
		<div class="col-md-12 table-responsive">
			<h1>Ramps:</h1>
			<table class="table table-bordered table-hover">
				<tr>
					<tr class="tg-s6z2" colspan="6">
						<td>ID</td>
						<td>Ramp name</td>
						<td>Status</td>
						<td>Ramp tel. number</td>
						<td>Location</td>
						<td>Map co-ordinates</td>
						<td>Interval</td>
						<td>Created on</td>
						<td>Updated at</td>
						<td></td>
					</tr>
				</tr>
				{foreach from=$ramps item=i}
					<tr>
						<td class="tg-vn2c">{$i.ramp_id}</td>
						<td class="tg-vn4c">{$i.ramp_name}</td>
						<td class="tg-vn4c">
							{if $i.status == 1 }
								<p class="alert-success">Active</p>
							{else}
								<p class="alert-danger">Inactive</p>
							{/if}
						</td>
						<td class="tg-vn4c">{$i.ramp_tel_number}</td>
						<td class="tg-vn4c">{$i.location}</td>
						<td class="tg-vn4c">{$i.map_coordinates_latitude}<br>{$i.map_coordinates_longitude}</td>
						<td class="tg-vn4c">{$i.interval}</td>
						<td class="tg-vn4c">{$i.created_at}</td>
						<td class="tg-vn4c">{$i.updated_at}</td>
						<td class="tg-vn4c"><a href="/ramps/{$i.ramp_id}" class="btn btn-danger">Edit</a></td>
					</tr>
				{/foreach}
			</table>
		</div>

		<div class="col-md-6">
			<div class="col-md-12">
				<div class="well well-sm">
					<form class="form-horizontal" method="post">
						<fieldset>
							<legend class="header col-md-10 col-md-offset-1 form-main-title">Add new ramp</legend>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-ramp-name">Ramp name:</label>
									<input id="input-ramp-name" name="ramp_name" type="text" placeholder="" class="form-control" required>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-ramp-tel">Ramp telephone number:</label>
									<input id="input-ramp-tel" name="ramp_tel_number" type="tel" placeholder="" class="form-control" required>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-ramp-interval">Ramp time delay interval (in seconds):</label>
									<input id="input-ramp-interval" name="interval" type="number" placeholder="" class="form-control" required>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label for="input-ramp-status">Ramp status:</label>
									<select name="status" id="input-ramp-status" class="form-control" required>
										<option value=1>Active</option>
										<option value=0>Inactive</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1" style="position:relative;">
									<label for="input-ramp-address">Ramp address:</label>
									<div class="input-group">
										<input id="address" name="location" placeholder="Ramp location" class="form-control" value="" required >
										<span class="input-group-btn">
											<button id="get-location" class="btn btn-default" type="button" value="Geocode" onclick="codeAddress()"><i class="fa fa-location-arrow"></i></button>
										</span>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<label class="input-title" for="input-ramp-coords">Ramp coordinates:</label>
									<input id="input-ramp-coords-lat" name="map_coordinates_latitude" type="text" placeholder="Latitude" class="form-control col-md-6 col-xs-12" required>
									<input id="input-ramp-coords-lng" name="map_coordinates_longitude" type="text" placeholder="Longitude" class="form-control col-md-6 col-xs-12" required>
									<div class="clear"></div>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-10 col-md-offset-1">
									<button id="submit-new-ramp" type="submit" class="btn btn-primary btn-lg">Submit</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		{$flash.rampe}

		<div id="map-canvas" class="col-md-6"></div>

		{block name=response}
			<div class="status-message-2 col-md-6">
				{if $flash.newRampAdded == 1}
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Success!</strong> New ramp added to database.
				</div>
				{/if}
			</div>
		{/block}
	</div>
</section>
{/block}
