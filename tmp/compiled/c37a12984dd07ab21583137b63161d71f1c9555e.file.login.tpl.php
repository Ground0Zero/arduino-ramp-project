<?php /* Smarty version Smarty-3.1.19, created on 2015-06-18 11:51:55
         compiled from "tpl/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:816104928558294bb43e969-58174029%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c37a12984dd07ab21583137b63161d71f1c9555e' => 
    array (
      0 => 'tpl/login.tpl',
      1 => 1434464723,
      2 => 'file',
    ),
    '958f57e5fa23ca734b5e880e1d5a7f20133d25c1' => 
    array (
      0 => 'tpl/index-without-nav.tpl',
      1 => 1434464723,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '816104928558294bb43e969-58174029',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'sitename' => 0,
    'route' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_558294bb48c296_80366263',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_558294bb48c296_80366263')) {function content_558294bb48c296_80366263($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
	<head>
		<TITLE><?php echo $_smarty_tpl->tpl_vars['sitename']->value;?>
 - 
	Login
</TITLE>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Bootstrap core CSS, Font Awesome, Custom styles -->
		<link href="/static/css/bootstrap.min.css" rel="stylesheet">
		<link href="/static/css/font-awesome.min.css" rel="stylesheet">
		<link href="/static/css/style.css" rel="stylesheet">

		<!-- Geo js -->
		<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
		<script src="/static/js/geo.js"></script>

	</head>

	<body class="page-<?php echo ltrim($_smarty_tpl->tpl_vars['route']->value,'/');?>
" data-route="<?php echo $_smarty_tpl->tpl_vars['route']->value;?>
">

	<div id="content">
			
	<section id="login">
		<div class="container">
			<div class="card card-container">
				<img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
				<p id="profile-name" class="profile-name-card"></p>
				<form method="post" class="form-signin">
					<span id="reauth-email" class="reauth-email"></span>
					<input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
					<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
					<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" value="Login">Login</button>
				</form>
				<?php if (!empty($_smarty_tpl->tpl_vars['error']->value)) {?>
					<p class="danger-msg-red"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p>
				<?php }?>
				<?php if (!empty($_smarty_tpl->tpl_vars['email_error']->value)) {?>
					<p class="danger-msg-red"><?php echo $_smarty_tpl->tpl_vars['email_error']->value;?>
</p>
				<?php }?>
				<?php if (!empty($_smarty_tpl->tpl_vars['password_error']->value)) {?>
					<p class="danger-msg-red"><?php echo $_smarty_tpl->tpl_vars['password_error']->value;?>
</p>
				<?php }?>
			</div>
		</div>
	</section>

	</div>

	<footer id="footer">
		<div class="wrap">
			<a href="/" target="_blank" class="footer-logo-1"></a>
			<a href="/" target="_blank" class="footer-logo-2"></a>
			<a href="/" target="_blank" class="footer-logo-3"></a>
			<a href="/" target="_blank" class="footer-logo-4"></a>
		</div>
	</footer>

	</body>

	<!-- jQuery, Bootstrap core JavaScript, Bootbox
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="/static/js/jquery-1.11.3.min.js"></script>
	<script src="/static/js/bootstrap.min.js"></script>
	<script src="/static/js/main.js"></script>
</html>
<?php }} ?>
