#include <Event.h>
#include <Timer.h>
#include <SPI.h>
#include <Ethernet.h>
#include <Servo.h>
//[{"action_id":1,"action_type":"up","action_status":"new","ramp_id":31,"user_id":17,"created_at":"-0001-11-30 00:00:00","updated_at":"-0001-11-30 00:00:00"}]
//Setup timer to run
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
char server[] = "192.168.0.222";

IPAddress ip(192,168,0,106);  // Set the static IP address to use if the DHCP fails to assign
EthernetClient client;
EthernetClient dclient;
Servo myservo; // create servo object to control a servo

String content = "";
String lastRampStatus ="";
unsigned long previousMillis = 0;
unsigned long lastConnectionTime = -30*1000;          // last time you connected to the server, in milliseconds
boolean lastConnected = false;                 // state of the connection last time through the main loop
const unsigned long postingInterval = 30*1000;


void setup() {
  Serial.begin(9600); // Open serial communications and wait for port to open:
    if (Ethernet.begin(mac) == 0) {
      Serial.println("Failed to configure Ethernet using DHCP");  // no point in carrying on, so do nothing forevermore:
      Ethernet.begin(mac, ip);  // try to congifure using IP address instead of DHCP:
    }
 delay(1000);  // give the Ethernet shield a second to initialize:
 myservo.attach(9);
}

void loop() {

 if (client.available()) { // while there is a data to be read - append it to "global" variable "content"
    char response = client.read();
    content += response;
 }
 
/* if (dclient.available()) {
   char c = dclient.read();
   Serial.print(c);
 } */

  // if there's no net connection, but there was one last time
  // through the loop, then stop the client:
 if (!client.connected() && lastConnected) {
   Serial.println();
   Serial.println("disconnecting.");
   client.stop();
   performAction(content);
 }
    // if you're not connected, and 3 seconds have passed since
    // your last connection, then connect again and send data:
 if (!client.connected() && (millis() - lastConnectionTime > postingInterval)) {
   httpRequest();
 }
  // store the state of the connection for next time through
  // the loop:
 lastConnected = client.connected();

}

void performAction(String content) {
  String action_id = getActionIDFromResponse(content);  // Get action_id from content
  String action_type = getActionTypeFromResponse(content);  // Get action_type from content

  Serial.println(content);
  Serial.println(action_id);
  Serial.println(action_type);

  if(action_type){ //if action id exists
    bool result = performServoAction(action_type, action_id);  //call function performServoAction with parameter action_type and store it into variable
       if(result == false){
         Serial.println("Ramp is already in that state");
         bool result2 = notifyServerAboutAction(result, action_id);  //Calling the functions and storing values into variables, notifying server by sending POST request
       }
       else {
         Serial.println("Ramp state has changed");
         bool result2 = notifyServerAboutAction(result, action_id);  //Calling the functions and storing values into variables, notifying server by sending POST request
       }
 }
 else {
   Serial.println("There are no actions");
 }
//while(true);  // do nothing forevermore:
}
// this method makes a HTTP connection to the server:
void httpRequest() {
  // if there's a successful connection:
  if (client.connect(server, 80)) {
    Serial.println("connecting...");
    // send the HTTP PUT request:
    client.println("GET http://rampa.local/api/actions?action_status=new&ramp_id=31");
   
    client.println();
   

    // note the time that the connection was made:
    lastConnectionTime = millis();
  }
  else {
    // if you couldn't make a connection:
    Serial.println("connection failed");
    Serial.println("disconnecting.");
    client.stop();
  }
}

String getActionIDFromResponse(String str) {
  int nstart = str.indexOf("action_id") + 11; // Get position of "action_id" in a string, increment by 11 (length of 'action_id":')
  int nend = str.indexOf(",", nstart); // Get next position of '"' starting from nstart
  return str.substring(nstart, nend); //Return substring between this position
}

String getActionTypeFromResponse(String str) {
  int nstart = str.indexOf("action_type") + 14; // Get position of "action_type" in a string, increment by 13 (length of 'action_type":"')
  int nend = str.indexOf("\"", nstart); // Get next position of '"' starting from nstart
  return str.substring(nstart, nend); //Return substring between this position
}

bool performServoAction(String action_type, String action_id) { //defining function to move servo
    if(action_type == lastRampStatus){ //if action type is the same as last servo action
        notifyServerAboutAction("false", action_id);  //call function sendActionLog
        return false;
    }
    
    else {
      if (action_type == "up") {
        Serial.println("doing up");
        myservo.write(45);
        lastRampStatus = action_type ;
        notifyServerAboutAction("true", action_id);
        return true;
      }

      if (action_type == "down") {
        Serial.println("doing down");
        myservo.write(135);
        lastRampStatus = action_type;
       notifyServerAboutAction("true", action_id);
        return true;
      }
   }
}

bool notifyServerAboutAction(bool result, String action_id) {
    String result_str = "completed";  
    if (result == false) {
      result_str = "failed";
     }
    
    String PostData = "data={\"action_status\": \"" + result_str + "\", \"action_id\": \"" + action_id + "\"}";
   
    
    if (dclient.connect(server, 80)) {  // if you get a connection, report back via serial:
      
      dclient.println("POST /api/actions HTTP/1.1");  // Make a HTTP request
      dclient.println("Host: rampa.local");
      dclient.println("Content-Type: application/x-www-form-urlencoded;");
      dclient.println("User-Agent: Arduino/1.0");
      dclient.println("Connection: close");
      dclient.print("Content-Length: ");
      dclient.println(PostData.length());
      dclient.println();
      dclient.println(PostData);
      Serial.println("notifying server about action");
      Serial.println("Server notified");
    }
    else {
      Serial.println("connection failed");
      Serial.println("disconnecting.");
    
    }
  return true;
}



