<?php
namespace App\Models;
use Illuminate\Database\Capsule\Manager as DB;

class Access extends \Illuminate\Database\Eloquent\Model
{
	public $timestamps = true;
	protected $table = 'access';
	protected $primaryKey = 'access_id';
	protected $guarded = array('access_id');

	//protected $fillable = ['user_id', 'ramp_id', 'user_permission'];

	public static function promote() {
		$this->promoted = true;
		$this->save();
	}
}

?>