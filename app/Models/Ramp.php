<?php
namespace App\Models;
use Illuminate\Database\Capsule\Manager as DB;

class Ramp extends \Illuminate\Database\Eloquent\Model
{
	public $timestamps = true;
	protected $table = 'ramp';
	protected $primaryKey = 'ramp_id';
	protected $guarded = array('ramp_id');


	public static function getWithCoordinates() {
		return self
			::select(
				'ramp_id', 'status', 'location', 'ramp_name', 'ramp_tel_number', 'interval', 'created_at', 'updated_at',
				DB::raw("SUBSTRING(AsText(map_coordinates), 7, LOCATE(' ', AsText(map_coordinates)) - 7) as map_coordinates_latitude"),
				DB::raw("SUBSTRING(AsText(map_coordinates), LOCATE(' ', AsText(map_coordinates)) + 1, LOCATE(')', AsText(map_coordinates)) - LOCATE(' ', AsText(map_coordinates)) - 1) as map_coordinates_longitude")
			);
	}

	public function setMapCoordinatesAttribute() {
		$this->attributes['map_coordinates'] = (DB::raw(
			'GeomFromText(
				\'POINT(
					'.$this->attributes['map_coordinates_latitude'].'
					'.$this->attributes['map_coordinates_longitude'].'
				)\'
			)'
		));

		unset($this->attributes['map_coordinates_latitude']);
		unset($this->attributes['map_coordinates_longitude']);
	}
}

?>