<?php
namespace App\Models;
use Illuminate\Database\Capsule\Manager as DB;

class User extends \Illuminate\Database\Eloquent\Model
{
	public $timestamps = true;
	protected $table = 'user';
	protected $primaryKey = 'user_id';
	protected $guarded = array('user_id');

	public static function findByMail($mail){
		return User::whereMail($mail)->get()->toArray();
	}
}

?>