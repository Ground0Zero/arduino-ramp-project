<?php
namespace App\Models;
use Illuminate\Database\Capsule\Manager as DB;

class Log extends \Illuminate\Database\Eloquent\Model
{
	public $timestamps = true;
	protected $table = 'action_log';
	protected $primaryKey = 'action_log_id';
	protected $guarded = array('action_log_id');

	public static function promote() {
		$this->promoted = true;
		$this->save();
	}
}

?>