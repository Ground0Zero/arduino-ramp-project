<?php
namespace App\Models;
use Illuminate\Database\Capsule\Manager as DB;

class Action extends \Illuminate\Database\Eloquent\Model
{
	public $timestamps = true;
	protected $table = 'action';
	protected $primaryKey = 'action_id';
	protected $guarded = array('action_id');

}

?>